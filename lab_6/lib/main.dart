import 'package:flutter/material.dart';

void main() {
  runApp(const NoteDote());
}

class NoteDote extends StatelessWidget {
  const NoteDote({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'NoteDote',
      theme: ThemeData(
        primarySwatch: Colors.grey,
        scaffoldBackgroundColor: Colors.white70,
      ),
      home: const ProfilePage(title: 'Profile'),
    );
  }
}

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(widget.title),
        ),
        drawer: MainDrawer(),
        body: Container(
            margin: const EdgeInsets.all(50.0),
            child: Center(
                child: Row(
              mainAxisAlignment:
                  MainAxisAlignment.center, //Center Row contents horizontally,
              crossAxisAlignment:
                  CrossAxisAlignment.center, //Center Row contents vertically,
              children: [
                Container(
                    margin: const EdgeInsets.all(5.0),
                    child: Image(
                      image: NetworkImage(
                          'https://flutter.github.io/assets-for-api-docs/assets/widgets/owl.jpg'),
                    )),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      'MyName',
                      style: TextStyle(fontSize: 25),
                    ),
                    Flexible(
                      child: new Container(
                        padding: new EdgeInsets.only(right: 13.0),
                        child: new Text(
                          'Hello!',
                          overflow: TextOverflow.ellipsis,
                          style: new TextStyle(fontSize: 12),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ))));
  }
}

class MainDrawer extends StatelessWidget {
  Widget buildListTile(String title, IconData icon) {
    return ListTile(
      leading: Icon(
        icon,
        size: 26,
      ),
      title: Text(
        title,
        style: TextStyle(
          fontFamily: 'RobotoCondensed',
          fontSize: 24,
          fontWeight: FontWeight.bold,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
        child: Container(
          color: Colors.grey,
      child: Column(
        children: <Widget>[
          SizedBox(
            height: 20,
          ),
          buildListTile('Dashboard', Icons.home),
          buildListTile('Personal Journal', Icons.laptop),
          buildListTile('Notes', Icons.folder),
          buildListTile('Quotes', Icons.text_fields),
          buildListTile('Weekly Schedule', Icons.calendar_today),
          buildListTile('Profile', Icons.info),
        ],
      ),
    ));
  }
}
