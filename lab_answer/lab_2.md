## 1. Apakah perbedaan antara JSON dan XML?
- JSON memliiki tipe data yaitu string, integer, array, dan boolean. Sedangkan XML hanya mempunyai string.
- Data pada JSON merupakan data yang siap dipakai (tidak usah melakukan parsing), sedangkan data pada XML perlu dilakukan parsing terlebih dahulu.
- JSON tidak mendukung namespace, sedangkan XML mendukung namespace.


## 2. Apakah perbedaan antara HTML dan XML?
Meskipun HTML (Hyper Text Markup Language) dan XML (eXtensible Markup Language) memiliki nama yang mirip, perbedaan keduanya tidaklah sedikit. 

- HTML digunakan untuk menampilkan data, atau untuk membuat page yang statis. Sedangkan XML digunakan dalam transportasi data.
- HTML merupakan markup languange, XML mempunyai framework yang mendefinisikan markup languange yang digunakan. Contohnya, XML menggunakan tag yang ditetapkan pengguna, sedangkan HTML tidak.
- HTML dirancang untuk menampilkan data, maka fokusnya terdapat pada bagaimana data terlihat, sedangkan XML dirancang untuk memindahkan data dengan fokusnya pada bagaimana data tersebut didefinisikan.
- HTML tidak case sensitve, sedangkan XML case sensitive. 

## Referensi
1. https://www.w3schools.com/js/js_json_xml.asp
2. https://www.guru99.com/json-vs-xml-difference.html
3. https://www.geeksforgeeks.org/html-vs-xml/