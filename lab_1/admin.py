from lab_1.models import Friend
from django.contrib import admin
from .models import Friend

admin.site.register(Friend)
