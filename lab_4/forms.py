from django.forms import ModelForm, Textarea
from lab_2.models import Note

class NoteForm(ModelForm):
    class Meta:
        model = Note
        fields =['to_name', 'from_name', 'title', 'message']
        widgets = {
          'message': Textarea(attrs={'rows':5, 'cols':40}),
        }