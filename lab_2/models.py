from django.db import models

class Note(models.Model):
    to_name = models.CharField(max_length=30)
    from_name = models.CharField(max_length=30)
    title = models.CharField(max_length=50)
    message = models.CharField(max_length=200)
